//MatLab applications in physics
//Author: Dawid Lazaj
//Technical Physics

//! Code written to work properly with scilab environment 
//! and will not work in matlab !
//! Program requires its script to be launched from the directory
//! that the 'data' folder is located !



listOfFiles = listfiles('data');//list of all elements located in 'data' folder
readListOfFiles = list();        //list to be filled with values from data file
maxPM10 = [0, 0];                                 //[maximum value, its indice]
day = 0;                                                  //date of measurement

elements = {list(), list()};           //{day measurements, night measurements}

//following loop go through 7 files and write content from
//'dane-pomiarowe_2019-10-....csv' as elements of 'readListOfFiles' list
//we acquire maxPM10 value and with its indice later on we'll find
//the hour of measurement
//assumption made: daytime <7:00, 19:00), nighttime <19:00, 7:00)
for x = 1:size(listOfFiles)(1);
    readListOfFiles(x) = csvRead('data\' + listOfFiles(x),...
                                 ';', ',', 'string', [',','.']);
    [temporaryValue, temporaryIndex] = max(strtod(readListOfFiles(x)(2:25,6)));
    elements{1}(x) = strtod(readListOfFiles(x)(8:19,6));
    elements{2}(x) = strtod(readListOfFiles(x)([2:7, 20:25],6));
    if temporaryValue > maxPM10(1) then;
        maxPM10 = [temporaryValue, temporaryIndex];
        day = x;
    end;
end;

meanValue = [0, 0];                        //[mean day value, mean night value]
dev = [0, 0];                                            //std deviation values

meanValue = [mean(list2vec(elements{1})), mean(list2vec(elements{2}))];
dev = [stdev(list2vec(elements{1})), stdev(list2vec(elements{2}))];

//getting standard deviation of the mean
//due to high quantity of measurements we can skip Student-Fisher coefficent
devMean = [dev(1)/sqrt(length(list2vec(elements{1}))),...
           dev(2)/sqrt(length(list2vec(elements{2})))];

//! compatibility test
t=abs(meanValue(2)-meanValue(1))/sqrt(devMean(2)**2 + devMean(1)**2);

//getting measurement's hour by reading from proper cell
//and second part of file's name, which is max measurement's data
hour=readListOfFiles(day)(maxPM10(2)+1,1);
date=strsplit(listOfFiles(day), ["_", "."])(2);

fileOpen = mopen('PMresults.dat', ['a']);        //opening 'PMresults.dat' file

mprintf('Highest measured PM10 concentration = %d on %s at %s o clock'+...
        ascii(10), maxPM10(1), date, hour);             //%s string, %d decimal
text1=msprintf('Highest measured PM10 concentration=%d on %s at %s o clock'+...
               ascii(10), maxPM10(1), date, hour);        //ascii(10) ends line
mfprintf(fileOpen, '%s', text1+ ascii(10));                  //writting in file

//checking if there's statistical difference between night and day 
if t >= 2 & t < 3 then;
    mprintf('No statistical difference between night and day in concentration of PM10. Compatibility test = %f'...
            + ascii(10), t);
    text2 = msprintf('No statistical difference between night and day in concentration of PM10. Compatibility test = %f' ...
                     + ascii(10), t);                                //%f float
    mfprintf(fileOpen, '%s', text2+ ascii(10));
else;
    mprintf('Statistical difference between night and day in concentration of PM10. Compatibility test =  %f'...
            + ascii(10), t);
    text2 = msprintf('Statistical difference between night and day in concentration of PM10. Compatibility test =  %f'...
                     + ascii(10), t);
    mfprintf(fileOpen, '%s', text2+ ascii(10));
end;

//displaying and writting in file informations about examined period
mprintf('Data taken from period since %s to %s' + ascii(10),...
        strsplit(listOfFiles(x), ["_", "."])(2), strsplit(listOfFiles(1),...
        ["_", "."])(2));
text3 = msprintf('Data taken from period since %s to %s' + ascii(10),...
                 strsplit(listOfFiles(x), ["_", "."])(2), ...
                 strsplit(listOfFiles(1), ["_", "."])(2));
mfprintf(fileOpen, '%s', text3+ ascii(10));

mclose(fileOpen); 
